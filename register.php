<?php
		include("usefull.php");
		include("connect.php");
		
		//fields
		$email = isset($_POST["email"]) ? strtolower($_POST["email"]) : "";
		$username = $_POST["username"];
		$pass = $_POST["password"];
		$pass_retype = $_POST["password_retype"];
		
		//referal check
		if (isset($_GET["partner"]) && $_GET["partner"]!=""){
			$_COOKIE["actual_referal"] = $_GET["partner"];
		}
		
		//pre DB errors
		$error_no_email = false;
		$error_no_username = false;
		$error_no_pass = false;
		$error_pass_not_match = false;
		$error_captcha = false;
		$error_email_invalid = false;
		$error_pass_too_small = false;
		$error_pass_too_large = false;
		
		//empty condition
		$condition_all_empty = true;
		
		
		//DB errors
		$error_repeated_email = false;
		$error_db_unknown = false;
		
		
		if ($email=="")
			$error_no_email = true;
		
		if ($username=="")
			$error_no_username = true;
		
		if ($pass=="" || $pass_retype=="")
			$error_no_pass = true;
		
		if ($email!="" || $username!="" || $pass!="" || $pass_retype!="")
			$condition_all_empty = false;
		
		if (strcmp($pass, $pass_retype)!=0)
			$error_pass_not_match = true;
		
		if (filter_var($email, FILTER_VALIDATE_EMAIL) === false && !$error_no_email)
			$error_email_invalid = true;
		
		if (strlen($pass)<5)
			$error_pass_too_small = true;
		
		if (strlen($pass)>30)
			$error_pass_too_large = true;
		
		
		//captcha check
		$remoteIp = $_SERVER['REMOTE_ADDR'];
		$gRecaptchaResponse = $_POST["g-recaptcha-response"];
		$secret = "6LckNRcUAAAAAEbuPeuL0tOL0EF5L2wVWQWyGYm7";
		//-full path to "autoload.php"
		require('autoload.php');
		$recaptcha = new \ReCaptcha\ReCaptcha($secret);
		$resp = $recaptcha->verify($gRecaptchaResponse, $remoteIp);
		
		if (!$resp->isSuccess())
			$error_captcha = true;
		
		//check if no pre db errors
		$preDbErrors = array(
			$error_no_email,
			$error_no_username,
			$error_no_pass,
			$error_pass_not_match,
			$error_captcha,
			$error_email_invalid,
			$error_pass_too_small,
			$error_pass_too_large
		);
		$error_trigger = false;
		for ($i = 0;$i<count($preDbErrors);$i++) 
			if ($preDbErrors[$i])
				$error_trigger = true;
		
		if (!$error_trigger){
			try {
				$DBH = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass);
				$DBH->exec('USE '.$db_name.';');	
				$DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
				
				$STH = $DBH->prepare("SELECT * FROM users WHERE login=?");
				$STH->execute(array($email));
				$data_obj = $STH->fetch();
				if (!$data_obj){
					$referal = (isset($_COOKIE["actual_referal"]) && $_COOKIE["actual_referal"]!="") ? $_COOKIE["actual_referal"] : "";
					$sessid = generate_session("sess");
					$STH = $DBH->prepare("INSERT INTO users (login, pass, session, firstname, date, referal) VALUES (?,?,?,?,?,?)");
					$STH->execute(array($email, md5(md5($pass)), $sessid, $username, date('Y-m-d'), $referal));
					setcookie("save_sessid", $sessid, time()+3600*24*365);
					localRedirect("cabinet.php");
					return;
				}else{
					$error_repeated_email = true;
				}
				
				
			}catch(PDOException $e) { 
				echo $e->getMessage();
				$error_db_unknown = true;
			}
		}
	?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title>Rocket4Apps</title>
	<link rel="stylesheet" href="css/style.css">
	<script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
</head>
<body>
	
	<div id="full-page-reg">
		<div class="login-bg">
			<div class="login-holder">
				<div class="login-container">
					<div class="login-top">
						<div class="lang-menu">
							<ul>
								<li><a href="#">en</a></li>
								<li class="active"><a href="#">ru</a></li>
							</ul>
						</div>
						<img src="img/logo.png" alt="" />
					</div>
					
					<div class="login-center">
						<div class="login-center-inner">
							<h1>Регистрация</h1>
							<form id="form1" method="post">
								<input type="email" name="email" class="input-email" placeholder="Ваш e-mail" value="<?php echo $email ?>" />
								<input type="text" name="username" class="input-login" placeholder="Ваше имя" value="<?php echo $username ?>" />
								<input type="password" name="password" class="input-pass" placeholder="Пароль" />
								<input type="password" name="password_retype" class="input-pass" placeholder="Повторите пароль" />
								<div class="captcha-container">
									<div id="RecaptchaField1"></div>
								</div>
								
								<?php 
									if ($error_no_email && !$condition_all_empty){echo "<p style=\"color:red\">Введите e-mail</p><br>";}
									else if ($error_email_invalid && !$condition_all_empty){echo "<p style=\"color:red\">Не верный e-mail</p><br>";}
									else if ($error_no_username && !$condition_all_empty){echo "<p style=\"color:red\">Введите имя</p><br>";}
									else if ($error_no_pass && !$condition_all_empty){echo "<p style=\"color:red\">Введите пароль</p><br>";}
									else if ($error_pass_too_small && !$condition_all_empty){echo "<p style=\"color:red\">Слишком простой пароль</p><br>";}
									else if ($error_pass_too_large && !$condition_all_empty){echo "<p style=\"color:red\">Слишком длинный пароль</p><br>";}
									else if ($error_pass_not_match && !$condition_all_empty){echo "<p style=\"color:red\">Пароли не совпадают</p><br>";}
									else if ($error_captcha && !$condition_all_empty){echo "<p style=\"color:red\">Нажмите на капчу</p><br>";}
									else if ($error_repeated_email && !$condition_all_empty){echo "<p style=\"color:red\">Этот e-mail уже занят</p><br>";}
									else if ($error_db_unknown && !$condition_all_empty){echo "<p style=\"color:red\">Unknown DB error</p><br>";}
								?>
								<button type="submit" class="btn btn-blue">ЗАРЕГИСТРИРОВАТЬСЯ</button>
								<a href="/" class="btn btn-yellow">ВОЙТИ В СИСТЕМУ</a>
							</form>
						</div>
					</div>
					
					<div class="login-bottom">
						<div class="login-bottom-holder">
							<div class="login-bottom-left">
								<h4>КОНТАКТЫ:</h4>
							</div>
							<div class="login-bottom-right">
								<p>Москва, Россия: +7 (499) 40-33-917<br/>
								Киев, Украина: +38 (063) 183-43-62</p>
								<p>e-mail: <a href="mailto:support@rocket4app.ru">support@rocket4app.ru</a></p>
								<p>skype: rocket4app</p>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
		var CaptchaCallback = function() {
				grecaptcha.render('RecaptchaField1', {'sitekey' : '6LckNRcUAAAAAA0XQ-2sA3U3qIBGQgbNbq0nIFqP'});
		};
	</script>
	
</body>
</html>
