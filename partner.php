<?php 
	include("usefull.php");
	include("connect.php");
	include("parts.php");

	$user_data = null;
	$partners_data = null;
	
	$sessid = isset($_COOKIE["save_sessid"]) ? $_COOKIE["save_sessid"] : "";
	
	if ($sessid!=""){
		try {
			$DBH = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass);
			$DBH->exec('USE '.$db_name.';');	
			$DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
			
			$STH = $DBH->prepare("SET NAMES 'utf8';SET CHARACTER SET 'utf8';SET SESSION collation_connection = 'utf8_general_ci';");
			$STH->execute();
			
			$STH = $DBH->prepare("SELECT * FROM users WHERE session=?");
			$STH->execute(array($sessid));
			$data_obj = $STH->fetch();
			
			if ($data_obj){
				$user_data = $data_obj;
				
				//parse partners
				
				$STH = $DBH->prepare("SELECT * FROM users WHERE referal=?");
				$STH->execute(array($user_data["id"]));
				$partners_data = $STH->fetchAll();
			
			
			}else{
				localRedirect("/");
			}
		}catch(PDOException $e) {  
			echo($e->getMessage());
		}
	}else{
		localRedirect("/");
	}
?>
<?php echo getHeader($user_data["firstname"],$user_data["balance"], "partner"); ?>

<div class="content-section">
	<div class="cs-head">
		<h2>ПАРТНЕРСКАЯ ПРОГРАММА</h2>
	</div>
	<div class="cs-body white-bg">
		<h3 class="referral-title">РЕФЕРАЛЬНЫЕ ССЫЛКИ</h3>
		<form class="blue-form bordered pc-bg">
			<div class="cs-short">
			<!--
				<label>
					На rocket4app.ru
					<input type="text" class="for-copy" value="http://<?php echo $_SERVER['HTTP_HOST'];?>/?partner=<?php echo $user_data["id"];?>" readonly="readonly" />
				</label>
				<label>
					На главную страницу
					<input type="text" class="for-copy" value="http://<?php echo $_SERVER['HTTP_HOST'];?>/cabinet.php?partner=<?php echo $user_data["id"];?>" readonly="readonly" />
				</label>
			-->
				<label>
					На страницу регистрации
					<input type="text" class="for-copy" value="https://<?php echo $_SERVER['HTTP_HOST'];?>/register.php?partner=<?php echo $user_data["id"];?>" readonly="readonly" />
				</label>
			</div>
		</form>
		<h3 class="invites-title">ПРИГЛАШЕННЫЕ РЕФЕРАЛЫ</h3>
		<table class="striped-table responsive-table">
			<tr>
				<th>E-mail</th>
				<th>Заработано</th>
				<th>Дата регистрации</th>
			</tr>
			<?php 
				for ($i=0;$i<count($partners_data);$i++)
					echo getReferalPart($partners_data[$i]["login"], $partners_data[$i]["referal_income"], $partners_data[$i]["date"]);
			?>
		</table>
	</div>
</div>

<?php echo getFooter(); ?>

