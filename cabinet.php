<?php
include ("usefull.php");
include ("connect.php");
include ("parts.php");
include ("google_play_api.php");

$user_data = null;
$apps_data = null;
$compaigns_data = null;
$hInstalls = null;
$dInstalls = null;
$allInstalls = null;
$edit_objcts = null;
$prices_obj = null;

$content = file_get_contents ( "keys.json" );
$keys = json_decode ( $content, true );

if (isset ( $_GET ["partner"] ) && $_GET ["partner"] != "") {
	$_COOKIE ["actual_referal"] = $_GET ["partner"];
}

$sessid = isset ( $_COOKIE ["save_sessid"] ) ? $_COOKIE ["save_sessid"] : "";
$app_url = isset ( $_POST ["app_url"] ) ? $_POST ["app_url"] : "";
$app_selector = isset ( $_POST ["app_selector"] ) ? $_POST ["app_selector"] : "";
$lang_selector = isset ( $_POST ["lang_selector"] ) ? $_POST ["lang_selector"] : "";
$keywords = isset ( $_POST ["keywords"] ) ? $_POST ["keywords"] : "";
$hi_limit = isset ( $_POST ["hi_limit"] ) ? $_POST ["hi_limit"] : "";
$di_limit = isset ( $_POST ["di_limit"] ) ? $_POST ["di_limit"] : "";
$mi_limit = isset ( $_POST ["mi_limit"] ) ? $_POST ["mi_limit"] : "";
$price = isset ( $_POST ["price"] ) ? $_POST ["price"] : "";

if (isset ( $_POST ["id_0"] ) && $_POST ["id_0"] != "") {
	$counter = 0;
	$objcts_arr = array ();
	while ( isset ( $_POST ["id_${counter}"] ) && $_POST ["id_${counter}"] != "" ) {
		$obj = new stdClass ();
		$obj->isRunning = (isset ( $_POST ["running_${counter}"] ) && $_POST ["running_${counter}"] != "") ? 1 : 0;
		$obj->hidden = (isset ( $_POST ["hidden_${counter}"] ) && $_POST ["hidden_${counter}"] != "") ? 1 : 0;
		$obj->hi_limit = $_POST ["hourLim_$counter"];
		$obj->di_limit = $_POST ["dayLim_$counter"];
		$obj->mi_limit = $_POST ["monthLim_$counter"];
		$obj->index = $_POST ["id_$counter"];
		$objcts_arr [] = $obj;
		$counter ++;
	}
	$edit_objcts = $objcts_arr;
}

if ($sessid != "") {
	try {
		$DBH = new PDO ( "mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass );
		$DBH->exec ( 'USE ' . $db_name . ';' );
		$DBH->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		
		$STH = $DBH->prepare ( "SET NAMES 'utf8';SET CHARACTER SET 'utf8';SET SESSION collation_connection = 'utf8_general_ci';" );
		$STH->execute ();
		
		$STH = $DBH->prepare ( "SELECT * FROM users WHERE session=?" );
		$STH->execute ( array (
				$sessid 
		) );
		$data_obj = $STH->fetch ();
		
		if ($data_obj) {
			$user_data = $data_obj;
			$STH = $DBH->prepare ( "SELECT * FROM prices WHERE user=?" );
			$STH->execute ( array (
					$user_data ["id"] 
			) );
			$prices_obj = $STH->fetch ();
			
			if ($prices_obj ["price_1_US"] == 0) {
				$STH = $DBH->prepare ( "SELECT * FROM prices WHERE user=?" );
				$STH->execute ( array (
						0 
				) );
				$prices_obj = $STH->fetch ();
			}
			
			init ( $DBH );
		} else {
			localRedirect ( "/" );
		}
	} catch ( PDOException $e ) {
		echo ($e->getMessage ());
	}
} else {
	localRedirect ( "/" );
}
function initData($DBH) {
	global $user_data, $apps_data, $compaigns_data, $adm_user, $hInstalls, $dInstalls, $allInstalls;
	
	// Apps data
	if ($user_data ["id"] == $adm_user && isset($_GET['show_all'])) {
		$STH = $DBH->prepare ( "SELECT * FROM apps ORDER BY id DESC" );
		$STH->execute ();
	} else {
		$STH = $DBH->prepare ( "SELECT * FROM apps WHERE user=? ORDER BY id DESC" );
		$STH->execute ( array (
				$user_data ["id"] 
		) );
	}
	$apps_data = $STH->fetchAll ();
	
	// Campaigns data
	if ($user_data ["id"] == $adm_user && isset($_GET['show_all'])) {
		$STH = $DBH->prepare ( "SELECT * FROM compaigns ORDER BY id ASC" );
		$STH->execute ();
	} else {
		$STH = $DBH->prepare ( "SELECT * FROM compaigns WHERE user=? ORDER BY id ASC" );
		$STH->execute ( array (
				$user_data ["id"] 
		) );
	}
	$compaigns_data = $STH->fetchAll ();
	
	// All users data
	$STH = $DBH->prepare ( "SELECT * FROM users" );
	$STH->execute ();
	$all_users_data = $STH->fetchAll ();
	
	$STH = $DBH->prepare ( "SELECT * FROM compaigns WHERE status=1 AND notfound<3 AND to_di_limit>0" );
	$STH->execute ();
	$work_compaigns_data = $STH->fetchAll ();
	
	$hInstalls = 0;
	$dInstalls = 0;
	$allInstalls = 0;
	
	for($i = 0; $i < count ( $work_compaigns_data ); $i ++) {
		for($k = 0; $k < count ( $all_users_data ); $k ++) {
			if ($work_compaigns_data [$i] [1] == $all_users_data [$k] [0] && $work_compaigns_data [$i] [1] != $adm_user) 
			{
				$hInst = $work_compaigns_data [$i] [6] - $work_compaigns_data [$i] [11];
				$hInst = $hInst > 0 ? $hInst : 0;
				$dInst = $work_compaigns_data [$i] [7] - $work_compaigns_data [$i] [12];
				$dInst = $dInst > 0 ? $dInst : 0;
				$allInst = $work_compaigns_data [$i] [8] - $work_compaigns_data [$i] [14];
				$allInst = $allInst > 0 ? $allInst : 0;
				$price = $work_compaigns_data [$i] [9];
				$bal = $all_users_data [$k] [7];
				$inst = round ( $bal / $price, 0 );
				
				// echo "hour " . $work_compaigns_data [$i] [6] . " " . $work_compaigns_data [$i] [11] . "<br/>";
				// echo "day " . $work_compaigns_data [$i] [7] . " " . $work_compaigns_data [$i] [12] . "<br/>";
				// echo "all " . $work_compaigns_data [$i] [8] . " " . $work_compaigns_data [$i] [14] . "<br/>";
				
				// echo "allInst" . $allInst . "<br/>";
				// echo "dInst" . $dInst . "<br/>";
				// echo "hInst" . $hInst . "<br/>";
				// echo "inst" . $inst . "<br/>";
				
				if ($bal > $price) {
					if ($allInst > $inst || $work_compaigns_data [$i] [8] == 0)
						$allInst = $inst;
					
					if ($allInst == 0)
						continue;
					
					if ($dInst > $allInst || $work_compaigns_data [$i] [7] == 0)
						$dInst = $allInst;
					
					if ($hInst > $dInst || $work_compaigns_data [$i] [6] == 0)
						$hInst = $dInst;
					
					$hInstalls = $hInstalls + $hInst;
					$dInstalls = $dInstalls + $dInst;
					$allInstalls = $allInstalls + $allInst;
				}
			}
		}
	}
}
function init($DBH) {
	global $user_data, $app_url, $apps_data, $app_selector, $lang_selector, $keywords, $hi_limit, $di_limit, $mi_limit, $price, $edit_objcts, $compaigns_data;
	
	initData ( $DBH );
	if ($app_url != "") {
		$new_app_url = explode ( "&hl", $app_url )[0] . "&hl=en";
		$app_obj = parseUrl ( $new_app_url );
		if ($app_obj != null) {
			if ($app_obj->app_name != "") {
				$STH = $DBH->prepare ( "INSERT INTO apps (user,uid,name,dev,url) VALUES (?,?,?,?,?)  
				ON DUPLICATE KEY UPDATE
              		name = VALUES(name),
              		dev = VALUES(dev)" );
				$STH->execute ( array (
						$user_data ["id"],
						$app_obj->uid,
						$app_obj->app_name,
						$app_obj->dev_name,
						$new_app_url 
				) );
				initData ( $DBH );
			}
		}
	} elseif ($app_selector != "") {
		for($i = 0; $i < count ( $apps_data ); $i ++) {
			if ($apps_data [$i] [0] == $app_selector) {
				$STH = $DBH->prepare ( "INSERT INTO compaigns (user,app,country,keywords,hi_limit,di_limit,mi_limit,price,proxy,rating,reviews) VALUES (?,?,?,?,?,?,?,?,?,?,?)" );
				$STH->execute ( array (
						$user_data ["id"],
						$apps_data [$i] [0],
						$lang_selector,
						$keywords,
						$hi_limit,
						$di_limit,
						$mi_limit,
						$price,
						$lang_selector, 3, "" 
				) );
			}
		}
		initData ( $DBH );
	} elseif ($edit_objcts != null) {
		for($i = 0; $i < count ( $edit_objcts ); $i ++) {
			
			$obj = $edit_objcts [$i];
			if (isCapmpaignExists ( $obj->index )) {
				$STH = $DBH->prepare ( "UPDATE compaigns SET status=?,hidden=?, hi_limit=?,di_limit=?,mi_limit=?  WHERE id=?" );
				$STH->execute ( array (
						$obj->isRunning,
						$obj->hidden,
						$obj->hi_limit,
						$obj->di_limit,
						$obj->mi_limit,
						$obj->index 
				) );
			}
		}
		initData ( $DBH );
	}
}
function isCapmpaignExists($id) {
	global $compaigns_data;
	for($i = 0; $i < count ( $compaigns_data ); $i ++) {
		if ($compaigns_data [$i] [0] == $id)
			return true;
	}
	return false;
}

?>

<?php echo getHeader($user_data["firstname"],$user_data["balance"], "cabinet"); ?>


<script>
		$(document).ready(function() {

			// If cookie is set, scroll to the position saved in the cookie.
			if ( $.cookie("scroll") !== null ) {
				$(document).scrollTop( $.cookie("scroll") );
				$.cookie("scroll", null);
			}

			// When a button is clicked...
			$('#submit').on("click", function() {

				// Set a cookie that holds the scroll position.
				$.cookie("scroll", $(document).scrollTop() );

			});


				$('#tab-'+$.cookie("tab")+'-li').find('a').trigger('click');


		});
	</script>

<div class="content-section">
	<div class="cs-head">
		<h2>ДОБАВИТЬ ПРИЛОЖЕНИЕ</h2>
	</div>
	<div class="cs-body">
		<div class="cs-short">
			<p>Вставьте ссылку на Ваше приложение в Google Play</p>
			<form id="form" method="post">
				<input type="text" placeholder="Ссылка на приложение в Google Play"
					name="app_url" />
				<button type="submit" class="btn btn-blue-square btn-save">Сохранить</button>
			</form>
			<script>
				$( "form" ).submit(function( event ) {
					$.cookie("scroll", $(document).scrollTop() );
				});
			</script>

			<?php if ($app_url!=""){echo "<font color=\"00AA00\">Готово<font/><font color=\"000000\"/>";}?>
		</div>
	</div>
</div>

<div class="content-section">
	<div class="cs-head">
		<h2>НОВАЯ КАМПАНИЯ</h2>
	</div>
	<div class="cs-body">
		<p>Введите поисковый запрос по которому будут идти установки. Чем
			больше поисковых установок - тем выше приложение в ПОИСКЕ по этому
			запросу.</p>
		<p>Либо оставьте поле пустым, для обычных установок. Чем больше
			установок - тем выше приложение в ТОПе.</p>
		<form method="post" id="form">
			<table class="form-table destroy-mobile">
				<tr>
					<td class="app-name-cell"><label> Приложение <select
							name="app_selector">
									<?php for ($i=0;$i<count($apps_data);$i++){$id = $apps_data[$i][0];$name = $apps_data[$i][3];echo "<option name=\"${id}\" value=\"${id}\">${name}</option>";}?>
								</select>
					</label></td>
					<td class="country-cell"><label> Страна <select id="lang_selector"
							name="lang_selector">
								<option name="US">US</option>
								<option name="RU">RU</option>
						</select>
					</label></td>
					<td class="app-name"><label> Поисковый запрос <input type="text"
							id="keywords" name="keywords"
							placeholder="Оставьте пустым, для обычных установок" />
					</label></td>
				</tr>
			</table>
			<hr />
			<input id="hide_price" type="hidden" name="price" value="1" /> <input
				id="price_1_US" type="hidden" name="price_1_US"
				value="<?php echo $prices_obj["price_1_US"]?>" /> <input
				id="price_2_US" type="hidden" name="price_2_US"
				value="<?php echo $prices_obj["price_2_US"]?>" /> <input
				id="price_1_RU" type="hidden" name="price_1_RU"
				value="<?php echo $prices_obj["price_1_RU"]?>" /> <input
				id="price_2_RU" type="hidden" name="price_2_RU"
				value="<?php echo $prices_obj["price_2_RU"]?>" />


			<script>
					function updatePrice(){
						if ($('#lang_selector').val() == "US") {
							if ($('#keywords').val() == "") {
								$("#price_txt").html("ОБЫЧНЫЙ ИНСТАЛЛ: <span>"+$("#price_1_US").val()+" USD</span>");
								$("#hide_price").val($("#price_1_US").val());
						    }
							else {
								$("#price_txt").html("ПОИСКОВЫЙ ИНСТАЛЛ: <span>"+$("#price_2_US").val()+" USD</span>");
						    	$("#hide_price").val($("#price_2_US").val());
							}
						}
						else {
							if ($('#keywords').val() == "") {
								$("#price_txt").html("ОБЫЧНЫЙ ИНСТАЛЛ: <span>"+$("#price_1_RU").val()+" USD</span>");
								$("#hide_price").val($("#price_1_RU").val());
						    }
							else {
								$("#price_txt").html("ПОИСКОВЫЙ ИНСТАЛЛ: <span>"+$("#price_2_RU").val()+" USD</span>");
						    	$("#hide_price").val($("#price_2_RU").val());
							}
						}
					}
					
					$(document).ready(function() {
						updatePrice();
						$('#keywords').on('input', updatePrice);
						$('#lang_selector').on('change', updatePrice);
					});
				</script>

			<!--
				<script>
					var countriesPrices = {
						"RU":$("#price_1_RU").val(),
						"US":$("#price_1_US").val()
					};

					var installAdditionPrices =
					{
						"RU":{
							"direct":0,
							"search":$("#price_2_RU").val()-$("#price_1_RU").val()
						},
						"US":{
							"direct":0,
							"search":$("#price_2_US").val()-$("#price_1_US").val()
						},
					};

					function updatePrice(){
						var keywords = $('#keywords').val();
						if (keywords!="") {
							var price = countriesPrices[$('#lang_selector').val()]*1+installAdditionPrices[$('#lang_selector').val()]["direct"]*1;
							$("#price_txt").html("ПОИСКОВЫЙ ИНСТАЛЛ: <span>"+price+" USD</span>");
							$("#hide_price").val(price);
						}
						else {
							var price = countriesPrices[$('#lang_selector').val()]*1+installAdditionPrices[$('#lang_selector').val()]["search"]*1;
							$("#price_txt").html("ПРЯМОЙ ИНСТАЛЛ: <span>"+price+" USD</span>");
							$("#hide_price").val(price);
						}
					}

					$(document).ready(function() {
						updatePrice();
						$('#keywords').on('input', updatePrice);
						$('#lang_selector').on('change', updatePrice);
					});
				</script>
-->
			<div class="table-with-price clearfix">
				<div class="price-table">
					<table class="form-table destroy-mobile">
						<tr>
							<td><label> Лимит в час <input type="text" name="hi_limit"
									placeholder="0" />
							</label></td>
							<td><label> Лимит в день <input type="text" name="di_limit"
									placeholder="0" />
							</label></td>
							<td><label> Лимит всего <input type="text" name="mi_limit"
									placeholder="0" />
							</label></td>
						</tr>
					</table>
				</div>
				<div id="price_txt" class="price-after-table">
					<span></span>
				</div>
			</div>

			<button type="submit" class="btn btn-blue-square btn-save">Сохранить</button>
		</form>
		<script>
			$( "form" ).submit(function( event ) {
				$.cookie("scroll", $(document).scrollTop() );
			});
		</script>
	</div>
</div>

<div class="tabs-outer">
	<div class="tabs clearfix">
		<ul class="for_tabs">
			<li id="tab-1-li"><a href="#tab-1">ВСЕ КАМПАНИИ</a></li>
			<li id="tab-2-li"><a href="#tab-2">ЗАПУЩЕННЫЕ КАМПАНИИ</a></li>
			<li id="tab-3-li"><a href="#tab-3">УДАЛЕННЫЕ КАМПАНИИ</a></li>
		</ul>

		<script>
				$('#tab-1-li').click(function () {
					$.cookie("tab", "1" );
				});

				$('#tab-2-li').click(function () {
					$.cookie("tab", "2" );
				});

				$('#tab-3-li').click(function () {
					$.cookie("tab", "3" );
				})
			</script>
		<div id='tab-1'>
			<form method="post" id="form">
				<button class="btn btn-blue-square btn-save">Сохранить</button>
				<table class="companies-list responsive-table">
					<tr>
						<th></th>
						<th class="cell-name text-left">Приложение</th>
						<th class="cell-country">Страна</th>
						<th class="cell-tags"></th>
						<th class="cell-number-options">Час/День/Всего <br />лимиты
							установок
						</th>
						<th>Сегодня<br />сделано
						</th>
						<th>Цена<br />установки
						</th>
						<th>Всего<br />сделано
						</th>
						<th>Удалить</th>
					</tr>
				<?php
				// All campaigns
				$index = 0;
				for($i = count ( $compaigns_data ) - 1; $i >= 0; $i --) {
					$isHidden = $compaigns_data [$i] [10];
					for($j = 0; $j < count ( $apps_data ); $j ++) {
						if ($apps_data [$j] [0] == $compaigns_data [$i] [2]) {
							
							$camp_id = $compaigns_data [$i] [0];
							$app_arr = $apps_data [$j];
							$compaign_arr = $compaigns_data [$i];
							$dev = $app_arr [4];
							$name = $app_arr [3];
							$status = $compaign_arr [3];
							$lang = $compaign_arr [4];
							$keywords = $compaign_arr [5];
							$hidden = $compaign_arr [10];
							$hourLimit = $compaign_arr [6];
							$dayLimit = $compaign_arr [7];
							$monthLimit = $compaign_arr [8];
							$todayInstalls = $compaign_arr [12];
							$price = $compaign_arr [9];
							$totalInstalls = $compaign_arr [14];
							$notfound = $compaign_arr [19];
							$rating = $compaign_arr [17];
							$reviews = $compaign_arr [20];
							
							if ($status == 0 && $isHidden ==  1)
								continue;
							
							$countriesArr = ($lang == "RU") ? array (
									"RU" 
							) : array (
									"US" 
							);
							
							echo getCompanyPart ( $status == 1, $name, $dev, $countriesArr, $keywords, $hourLimit, $dayLimit, $monthLimit, $todayInstalls, $price, $totalInstalls, $isHidden, $index, $camp_id, $notfound );
							$index ++;
							break;
						}
					}
				}
				
				?>
			</table>

				<button type="submit" class="btn btn-blue-square btn-save">Сохранить</button>
			</form>

			<script>
				$( "form" ).submit(function( event ) {
					$.cookie("scroll", $(document).scrollTop() );
				});
			</script>
		</div>


		<div id='tab-2'>
			<form method="post" id="form">
				<button class="btn btn-blue-square btn-save">Сохранить</button>
				<table class="companies-list responsive-table">
					<tr>
						<th></th>
						<th class="cell-name text-left">Приложение</th>
						<th class="cell-country">Страна</th>
						<th class="cell-tags"></th>
						<th class="cell-number-options">Час/День/Всего <br />лимиты
							установок
						</th>
						<th>Сегодня<br />сделано
						</th>
						<th>Цена<br />установки
						</th>
						<th>Всего<br />сделано
						</th>
						<th>Удалить</th>
					</tr>
				<?php
				// running campaigns
				$index = 0;
				for($i = count ( $compaigns_data ) - 1; $i >= 0; $i --) {
					$isHidden = $compaigns_data [$i] [10];
					for($j = 0; $j < count ( $apps_data ); $j ++) {
						if ($apps_data [$j] [0] == $compaigns_data [$i] [2]) {
							
							$camp_id = $compaigns_data [$i] [0];
							$app_arr = $apps_data [$j];
							$compaign_arr = $compaigns_data [$i];
							$dev = $app_arr [4];
							$name = $app_arr [3];
							$status = $compaign_arr [3];
							$lang = $compaign_arr [4];
							$keywords = $compaign_arr [5];
							$hidden = $compaign_arr [10];
							$hourLimit = $compaign_arr [6];
							$dayLimit = $compaign_arr [7];
							$monthLimit = $compaign_arr [8];
							$todayInstalls = $compaign_arr [12];
							$price = $compaign_arr [9];
							$totalInstalls = $compaign_arr [14];
							$notfound = $compaign_arr [19];
							$rating = $compaign_arr [17];
							$reviews = $compaign_arr [20];
							
							if ($status == 0)
								continue;
							
							$countriesArr = ($lang == "RU") ? array (
									"RU" 
							) : array (
									"US" 
							);
							
							echo getCompanyPart ( $status == 1, $name, $dev, $countriesArr, $keywords, $hourLimit, $dayLimit, $monthLimit, $todayInstalls, $price, $totalInstalls, $isHidden, $index, $camp_id, $notfound );
							$index ++;
							break;
						}
					}
				}
				
				?>
			</table>

				<button type="submit" class="btn btn-blue-square btn-save">Сохранить</button>
			</form>


			<script>
				$( "form" ).submit(function( event ) {
					$.cookie("scroll", $(document).scrollTop() );

				});
			</script>
		</div>
		<div id='tab-3'>
			<form method="post" id="form">
				<button class="btn btn-blue-square btn-save">Сохранить</button>
				<table class="companies-list responsive-table">
					<tr>
						<th></th>
						<th class="cell-name text-left">Приложение</th>
						<th class="cell-country">Страна</th>
						<th class="cell-tags"></th>
						<th class="cell-number-options">Час/День/Всего <br />лимиты
							установок
						</th>
						<th>Сегодня<br />сделано
						</th>
						<th>Цена<br />установки
						</th>
						<th>Всего<br />сделано
						</th>
						<th>Удаленные</th>
					</tr>
				<?php
				// hidden campaigns
				$index = 0;
				for($i = count ( $compaigns_data ) - 1; $i >= 0; $i --) {
					$isHidden = $compaigns_data [$i] [10];
					for($j = 0; $j < count ( $apps_data ); $j ++) {
						if ($apps_data [$j] [0] == $compaigns_data [$i] [2]) {
							
							$camp_id = $compaigns_data [$i] [0];
							$app_arr = $apps_data [$j];
							$compaign_arr = $compaigns_data [$i];
							$dev = $app_arr [4];
							$name = $app_arr [3];
							$status = $compaign_arr [3];
							$lang = $compaign_arr [4];
							$keywords = $compaign_arr [5];
							$hidden = $compaign_arr [10];
							$hourLimit = $compaign_arr [6];
							$dayLimit = $compaign_arr [7];
							$monthLimit = $compaign_arr [8];
							$todayInstalls = $compaign_arr [12];
							$price = $compaign_arr [9];
							$totalInstalls = $compaign_arr [14];
							$notfound = $compaign_arr [19];
							$rating = $compaign_arr [17];
							$reviews = $compaign_arr [20];
							
							if ($status == 1 || $isHidden == 0)
								continue;
							
							$countriesArr = ($lang == "RU") ? array (
									"RU" 
							) : array (
									"US" 
							);
							
							echo getCompanyPart ( $status == 1, $name, $dev, $countriesArr, $keywords, $hourLimit, $dayLimit, $monthLimit, $todayInstalls, $price, $totalInstalls, $isHidden, $index, $camp_id, $notfound );
							$index ++;
							break;
						}
					}
				}
				
				?>
			</table>

				<button type="submit" class="btn btn-blue-square btn-save">Сохранить</button>
			</form>

			<script>
				$( "form" ).submit(function( event ) {
					$.cookie("scroll", $(document).scrollTop() );

				});
			</script>
		</div>
	</div>
</div>
<?php echo getFooter(); ?>